# ETL - Extract, Transform and Load
*Fraud detection (ETL) using Python, RegEx and SQL Database*

## Project Overview

## Background

Fraud is everywhere these days—whether you are a small shop or a large international business. While there are emerging technologies that employ machine learning and artificial intelligence to detect fraud, many instances of fraud detection still require strong data analytics.

Application of SQL skills to analyze historical card transactions and dimension tables in order to identify possible fraudulent case.

Accomplish three main tasks:

1. [Data Modeling](#Data-Modeling):
Define a database model to store the credit card data and create a SQL data warehouse using your model.

2. [Data Engineering](#Data-Engineering): Create a data warehouse schema on SQLite and populate your database from the XLSX, TXT, SQL files provided.
This project focuses on *ETL** or **Extract, Transform and Load** process and includes the following:

-	**Extracting** data from two different sources. 
-	**Transforming** data using SQL, Python, Pandas and Python RegEx module.
-	**Loading** data using SQLite to host final data set.

3. [Data Analysis](#Data-Analysis): System builds required reports (data mart) to analyze the data to identify possible fraudulent case.

---

## Files

### Python Files

* [__init__.py](py_scripts/__init__.py)
* [etl_data_mart.py](py_scripts/etl_data_mart.py)
* [etl_extraction_transformation_data.py](py_scripts/etl_extraction_transformation_data.py)
* [etl_load_db.py](py_scripts/etl_load_db.py)
* [etl_metadata.py](py_scripts/etl_metadata.py)
* [etl_tools.py](py_scripts/etl_tools.py)

### Query Files

* [etl_schema.sql](sql_scripts/etl_schema.sql)
* [etl_stg_drop.sql](sql_scripts/etl_stg_drop.sql)

### SQL Files

* [ddl_dml.sql](./ddl_dml.sql)

### Cron job every day

* [main.cron](./main.cron)

### TXT, XLSX Files

* [data.zip](./data.zip)


### Data Modeling

Create an entity relationship diagram (ERD) by inspecting the provided XLSX, TXT, SQL files.

Tool used to develop ERD [Quick Database Diagrams](https://app.quickdatabasediagrams.com/#/) to create your model.

![ERD](resources/ERD.png)


### Data Engineering

After creating the database schema, import the data from the corresponding XLSX, TXT, SQL files. 

The goal of this stage is to create automated pipeline that extracts, transform and loads data. For this purpose, we can launched system by schedule, by example, use of Cron.

This stage consists of several parts, where each step is building up from beginning of extracting data and function testing, through transformation and cleaning process to its final step connect and load to the database.

Tables in SCD2 forms: 
-	terminals, 
-	cards, 
-	accounts, 
-	clients.

Fact tables: 
-	passport_blacklist, 
-	transactions.

ETL and OLAP Schematic:

![ETL and OLAP](graphics/ETL.png)


### Data Analysis

Now that your data is prepared within the database, it's finally time to identify fraudulent transactions using SQL.

Fraud detection:

-	1) Searching passport fraud.

-	2) Searching account fraud.

-	3) Searching different city fraud.

-	4) Searching sum up fraud.

![example_fraud](resources/fraud.png)


## Resources

Enviroment:
-	Python 3.10.5.

Dependencies:
-	Please see [req.txt](req.txt) for complete list of dependencies.

Software:
-	Sublime Text3.
-	DB Browser for SQLite.
-	SQLite.

## Useful Resources
- [SCD](https://habr.com/ru/post/101544/)
- [SQLite](https://www.sqlitetutorial.net/)
- [RegEx](https://www.programiz.com/python-programming/regex)
- [Cron Examples](https://crontab.guru/examples.html)